import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

@RunWith(JUnit4.class)
public class SampleTest {
    private Sample testTarget;

    @Before
    public void setUp() {
        testTarget = new Sample();
    }

    @Test
    public void test() {
        String result = testTarget.execute();
        assertThat(result, is("ok"));
    }
}
